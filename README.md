## Docker 

Pour lancer les différents services docker, il faut utiliser la commande docker-compose up --build

Le docker-compose.yml décrit la stack à lancer: un container 'back' buildé a partir du Dockerfile présent dans le répertoire 'server', et un container 'front' buildé à partir du Dockerfile présent dans le répertoire 'client'.

## Front Port 4200

La partie Front va réaliser des appels sur l'api Express créée coté back (port 3000).
Pour cela le fichier proxy.json, va permettre au front pour faire des appels sur d'autre ports et surtout vers d'autre services.

Le dockerFile coté front est la porte d'entrée pour builder le service Front, une fois  avoir installé les dépendances nodes, le script dockerfile-entrypoint.sh sera exécuté afin de rendre le service actif (c'est a ce moment la que le proxy est défini pour les appels extérieurs à ce service)

## Back Port 3000

Pour la partie Back, le dockerfile du répertoire server sera l'unique permettant de builder l'image, installer les dépendances et lancer le serveur en exécutant le script index.js

Le service n'est pas accessible depuis l'exterieur, mais uniquement a l'intérieur du réseau docker
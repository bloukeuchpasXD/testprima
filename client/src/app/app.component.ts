import { Component } from '@angular/core';
import { ServerService } from './server.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ServerService]

})
export class AppComponent {

  constructor(public appService: ServerService) { }
  title = 'testPrimaa';

  getDataServer() {
    this.appService.getData().subscribe(result => {
      alert(result);
    });
  }
}
